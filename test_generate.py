#!/usr/bin/python3

import unittest

import heartfuzz


class TestGenerate(unittest.TestCase):
    @heartfuzz.given(dicom=heartfuzz.DICOM_valid())
    def test_simple_generate(self, dicom):
        print(dicom.__dict__)


if(__name__ == "__main__"):
    unittest.main()
