#!/usr/bin/python3

import hypothesis
import hypothesis.strategies
import hypothesis.extra
import hypothesis.extra.numpy

import pydicom
import pydicom.dataset


hypothesis.settings.register_profile("_", hypothesis.settings(suppress_health_check=[hypothesis.HealthCheck.too_slow]))
hypothesis.settings.load_profile("_")


def given(*args, **kwargs):
    return hypothesis.given(*args, **kwargs)

def assume(*args, **kwargs):
    return hypothesis.given(*args, **kwargs)

def builds(*args, **kwargs):
    return hypothesis.strategies.builds(*args, **kwargs)


def none():
    return hypothesis.strategies.none()

def bools():
    return hypothesis.strategies.booleans()


def uids():
    return hypothesis.strategies.from_regex("[\.0-9]{0,64}", fullmatch=True)

def long_strings():
    return hypothesis.strategies.from_regex("[\x20-\x7E]{0,64}", fullmatch=True)

def short_strings():
    return hypothesis.strategies.from_regex("[\x20-\x7E]{0,16}", fullmatch=True)

def date_strings():
    return hypothesis.strategies.dates().map(lambda x: x.strftime("%Y%m%d"))

def time_strings():
    return hypothesis.strategies.datetimes().map(lambda x: x.strftime("%H%M%S.%f"))

def date_time_strings():
    return hypothesis.strategies.datetimes().map(lambda x: x.strftime("%Y%m%d%H%M%S.%f"))

def code_strings():
    return hypothesis.strategies.from_regex("[A-Z0-9 -]{0,20}", fullmatch=True)

def names():
    return long_strings()

def int_strings():
    return hypothesis.strategies.from_regex("[-+]?[0-9]{1,12}", fullmatch=True)

def byte_strings():
    return hypothesis.strategies.from_regex(b".*?")


class DICOM():
    def __init__(self, file_path,
                       meta_sop_class_uid, meta_sop_instance_uid, meta_implementation_class_uid,
                       data_modality, data_transfer_syntax_uid, data_little_endian, data_implicit_vr,
                       data_content_date, data_content_time,
                       data_patient_name, data_patient_id, data_patient_birth_date, data_patient_sex,
                       data_study_instance_uid, data_series_instance_uid, data_sop_instance_uid, data_sop_class_uid,
                       data_study_description, data_study_id, data_study_date, data_study_time,
                       data_accession_number, data_referring_physician,
                       data_series_number, data_acquisition_date_time, data_series_description,
                       data_mime_type, data_volume,
                       data_instance_number, data_conversion_type, data_burned_in_annotation,
                       data_private_manufacturer, data_private_originator_name, data_private_originator_site, data_private_originator_id):
        self.file_path = file_path

        self.meta = pydicom.dataset.Dataset()
        self.meta.MediaStorageSOPClassUID = meta_sop_class_uid
        self.meta.MediaStorageSOPInstanceUID = meta_sop_instance_uid
        self.meta.ImplementationClassUID = meta_implementation_class_uid


        self.data = pydicom.dataset.FileDataset(file_path, {}, file_meta=self.meta, preamble=(b"\0"*128))

        self.data.Modality = data_modality
        self.data.file_meta.TransferSyntaxUID = data_transfer_syntax_uid

        self.data.is_little_endian = data_little_endian
        self.data.is_implicit_vr = data_implicit_vr

        self.data.ContentData = data_content_date
        self.data.ContentTime = data_content_time

        self.data.PatientName = data_patient_name
        self.data.PatientID = data_patient_id
        self.data.add_new(0x0010030, "DA", data_patient_birth_date)
        self.data.add_new(0x0010040, "CS", data_patient_sex)

        self.data.StudyInstanceUID = data_study_instance_uid
        self.data.SeriesInstanceUID = data_series_instance_uid
        self.data.SOPInstanceUID = data_sop_instance_uid
        self.data.SOPClassUID = data_sop_class_uid

        self.data.add_new(0x00081030, "LO", data_study_description)
        self.data.add_new(0x00200010, "SH", data_study_id)
        self.data.add_new(0x00080020, "DA", data_study_date)
        self.data.add_new(0x00080030, "TM", data_study_time)
        self.data.add_new(0x00080050, "SH", data_accession_number)

        self.data.add_new(0x00080090, "PN", data_referring_physician)

        self.data.add_new(0x00200011, "IS", data_series_number)
        self.data.add_new(0x0008002A, "DT", data_acquisition_date_time)
        self.data.add_new(0x0008103E, "LO", data_series_description)
        self.data.add_new(0x00420012, "LO", data_mime_type)
        self.data.add_new(0x00420011, "OB", data_volume)

        self.data.add_new(0x00200013, "IS", data_instance_number)
        self.data.add_new(0x00080064, "CS", data_conversion_type)
        self.data.add_new(0x00280301, "CS", data_burned_in_annotation)

        self.data.add_new(0x00080070, "LO", data_private_manufacturer)
        self.data.add_new(0x31410010, "LO", data_private_originator_name)
        self.data.add_new(0x31411010, "LO", data_private_originator_site)
        self.data.add_new(0x31411020, "LO", data_private_originator_id)


def DICOM_valid():
    return builds(DICOM, file_path=long_strings(),
                         meta_sop_class_uid=uids(), meta_sop_instance_uid=uids(), meta_implementation_class_uid=uids(),
                         data_modality=code_strings(),
                         data_transfer_syntax_uid=uids(), data_little_endian=bools(), data_implicit_vr=bools(),
                         data_content_date=date_strings(), data_content_time=time_strings(),
                         data_patient_name=names(), data_patient_id=long_strings(), data_patient_birth_date=date_strings(), data_patient_sex=code_strings(),
                         data_study_instance_uid=uids(), data_series_instance_uid=uids(), data_sop_instance_uid=uids(), data_sop_class_uid=uids(),
                         data_study_description=long_strings(), data_study_id=short_strings(), data_study_date=date_strings(), data_study_time=time_strings(),
                         data_accession_number=short_strings(), data_referring_physician=names(),
                         data_series_number=int_strings(), data_acquisition_date_time=date_time_strings(), data_series_description=long_strings(),
                         data_mime_type=long_strings(), data_volume=byte_strings(),
                         data_instance_number=int_strings(), data_conversion_type=code_strings(), data_burned_in_annotation=bools(),
                         data_private_manufacturer=long_strings(), data_private_originator_name=long_strings(),
                         data_private_originator_site=long_strings(), data_private_originator_id=long_strings())
